package com.example.rothnaksomang.mvp.ui.login.presenter;

import com.example.rothnaksomang.mvp.entity.User;

import java.util.List;

public interface Presenter {
    interface LoginView{
        void onShowProgressBar();
        void onHideProgressBar();
        void onUsernameError();
        void onPasswordError();
        void onLoginSuccess();
        void onLoginNotSuccess();
    }
    interface LoginPresenter{
        void onDestroyView();
        User onLoginCredential(User user,List<User> userList);
    }
    interface RegisterView{
        void onRegisterSuccess();
        void onUserError();
        void onUsernameRegisterError();
        void onPasswordRegisterError();
    }
    interface RegisterPresenter{
        void onRegisterCredential(User user, List<User> userList);
    }
}
