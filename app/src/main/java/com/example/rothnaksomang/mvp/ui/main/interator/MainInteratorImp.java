package com.example.rothnaksomang.mvp.ui.main.interator;

import android.text.TextUtils;

import com.example.rothnaksomang.mvp.entity.User;

public class MainInteratorImp implements Interator {
    @Override
    public void getUser(User user, GetUserResponceInterator getUserResponceInterator) {
        if(user==null){
            getUserResponceInterator.onUserError();
        }else{
            getUserResponceInterator.onGetUserSuccess();
        }
    }
}
