package com.example.rothnaksomang.mvp.ui.main.view.adapter;

import android.content.Context;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.rothnaksomang.mvp.R;
import com.example.rothnaksomang.mvp.entity.User;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class UserAdapter extends RecyclerView.Adapter<UserAdapter.UserViewHolder> {
    Context context;
    LayoutInflater inflater;
    List<User> users;
    User user;


    public UserAdapter(Context context, List<User> users,User user){
        this.context=context;
        this.users=users;
        this.user=user;
        inflater=LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public UserViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view=inflater.inflate(R.layout.layout_item,null);
        return new UserViewHolder(view);
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    @Override
    public void onBindViewHolder(@NonNull UserViewHolder userViewHolder, int i) {
        int colorCurrentUser=context.getResources().getColor(R.color.colorAccent);
        int colorOtherUser=context.getResources().getColor(R.color.colorPrimary);
//  set Property RelativeLayout in Java Code
        RelativeLayout.LayoutParams paramCardView =(RelativeLayout.LayoutParams)userViewHolder.cardView.getLayoutParams();
        RelativeLayout.LayoutParams paramProfile =(RelativeLayout.LayoutParams)userViewHolder.ivProfile.getLayoutParams();
        RelativeLayout.LayoutParams paramPostDate =(RelativeLayout.LayoutParams)userViewHolder.tvPostDate.getLayoutParams();

        if(user.getUsername().equals(users.get(i).getUsername()) && user.getPassword().equals(users.get(i).getPassword())){
            Log.e("oooo1"," User Adapter:"+user.getUsername());
//  set background color in Java Code
            userViewHolder.cardView.setCardBackgroundColor(colorCurrentUser);
//  set Property RelativeLayout in Java Code
            paramCardView.removeRule(RelativeLayout.RIGHT_OF);
            paramCardView.addRule(RelativeLayout.LEFT_OF,R.id.iv_profile);
            userViewHolder.cardView.setLayoutParams(paramCardView);

            paramProfile.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
            paramProfile.removeRule(RelativeLayout.ALIGN_PARENT_LEFT);
            userViewHolder.ivProfile.setLayoutParams(paramProfile);

            paramPostDate.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
            paramPostDate.removeRule(RelativeLayout.ALIGN_PARENT_RIGHT);
            userViewHolder.tvPostDate.setLayoutParams(paramPostDate);

        }else{
//  set background color in Java Code
            userViewHolder.cardView.setCardBackgroundColor(colorOtherUser);
//  set Property RelativeLayout in Java Code
            paramProfile.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
            paramProfile.removeRule(RelativeLayout.ALIGN_PARENT_RIGHT);
            userViewHolder.ivProfile.setLayoutParams(paramProfile);

            paramCardView.addRule(RelativeLayout.RIGHT_OF,R.id.iv_profile);
            paramCardView.removeRule(RelativeLayout.LEFT_OF);
            userViewHolder.cardView.setLayoutParams(paramCardView);

            paramPostDate.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
            paramPostDate.removeRule(RelativeLayout.ALIGN_PARENT_LEFT);
            userViewHolder.tvPostDate.setLayoutParams(paramPostDate);

        }

        userViewHolder.tvMessage.setText(users.get(i).getMesssage());
        userViewHolder.tvName.setText(users.get(i).getUsername());
        userViewHolder.tvPostDate.setText(users.get(i).getSendDate());
    }

    @Override
    public int getItemCount() {
        return users.size();
    }

    class UserViewHolder extends RecyclerView.ViewHolder {
//        @BindView(R.id.tvName)
        TextView tvMessage;
//        @BindView(R.id.tvGender)
        TextView tvPostDate;
        TextView tvName;
        CardView cardView;
        CircleImageView ivProfile;

        public UserViewHolder(@NonNull View itemView) {
            super(itemView);
//            ButterKnife.bind(this,itemView);
            tvMessage=itemView.findViewById(R.id.tv_message);
            tvPostDate=itemView.findViewById(R.id.tv_post_date);
            tvName=itemView.findViewById(R.id.tv_name);
            cardView=itemView.findViewById(R.id.card_view);
            ivProfile=itemView.findViewById(R.id.iv_profile);
        }
    }
}
