package com.example.rothnaksomang.mvp.entity;

import android.os.Parcel;
import android.os.Parcelable;

public class User implements Parcelable {
    private String username;
    private String password;
    private String messsage;
    private String sendDate;
    private String imageProfile;

    public User(){}

    public User(String username, String password, String messsage, String sendDate, String imageProfile) {
        this.username = username;
        this.password = password;
        this.messsage = messsage;
        this.sendDate = sendDate;
        this.imageProfile = imageProfile;
    }

    protected User(Parcel in) {
        username = in.readString();
        password = in.readString();
        messsage = in.readString();
        sendDate = in.readString();
        imageProfile = in.readString();
    }

    public static final Creator<User> CREATOR = new Creator<User>() {
        @Override
        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMesssage() {
        return messsage;
    }

    public void setMesssage(String messsage) {
        this.messsage = messsage;
    }

    public String getSendDate() {
        return sendDate;
    }

    public void setSendDate(String sendDate) {
        this.sendDate = sendDate;
    }

    public String getImageProfile() {
        return imageProfile;
    }

    public void setImageProfile(String imageProfile) {
        this.imageProfile = imageProfile;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(username);
        parcel.writeString(password);
        parcel.writeString(messsage);
        parcel.writeString(sendDate);
        parcel.writeString(imageProfile);
    }
}
