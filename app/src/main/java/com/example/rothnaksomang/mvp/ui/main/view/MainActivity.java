package com.example.rothnaksomang.mvp.ui.main.view;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.EditText;
import android.widget.Toast;

import com.example.rothnaksomang.mvp.R;
import com.example.rothnaksomang.mvp.base.BaseActivity;
import com.example.rothnaksomang.mvp.entity.User;
import com.example.rothnaksomang.mvp.ui.main.view.adapter.UserAdapter;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;

public class MainActivity extends BaseActivity {

    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference;
    List<User> userList;
    UserAdapter userAdapter;
    User currentUser;


    @BindView(R.id.et_message)
    EditText etMessage;
    @BindView(R.id.rv_chat)
    RecyclerView rvChat;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        userList=new ArrayList<>();
        currentUser=new User();
        firebaseDatabase=FirebaseDatabase.getInstance();
        databaseReference=firebaseDatabase.getReference("users");
        getCurrentUser();
        getUser();
//        setValue();

    }

    @OnClick(R.id.btn_submit)
    public void submit(){
        saveUser();
    }

    public void saveUser(){
        Date date=Calendar.getInstance().getTime();
        SimpleDateFormat df=new SimpleDateFormat("dd-MMM-yyyy HH:mm");
        String formattedDate=df.format(date);

        Log.e("ooo7","Date:"+formattedDate);

        String id=databaseReference.push().getKey();
        String message=etMessage.getText().toString();
        User user=new User();
        user.setUsername(currentUser.getUsername());
        user.setPassword(currentUser.getPassword());
        user.setMesssage(message);
        user.setSendDate(formattedDate);

        databaseReference.child(id).setValue(user);
        etMessage.setText("");
        Toasty.success(getApplicationContext(),"save success",Toast.LENGTH_SHORT,true).show();
    }
    public void getUser(){
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                userList.clear();
                for (DataSnapshot data:dataSnapshot.getChildren()) {
                    User user=data.getValue(User.class);
//              get user have message only
                    if(user.getMesssage()!=null){
                        userList.add(user);
                    }
                }
                setValue();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public void setValue(){
        userAdapter=new UserAdapter(this,userList,currentUser);
        rvChat.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));
        rvChat.setAdapter(userAdapter);
        rvChat.smoothScrollToPosition(rvChat.getAdapter().getItemCount());
    }
    public void getCurrentUser(){
        Intent intent=getIntent();
        currentUser=intent.getParcelableExtra("user");
        Log.e("ooo2","CurrentUser:"+currentUser.getUsername());
    }
}
