package com.example.rothnaksomang.mvp.ui.main.interator;

import com.example.rothnaksomang.mvp.entity.User;

public interface Interator {
    void getUser(User user, GetUserResponceInterator getUserResponceInterator);

    interface GetUserResponceInterator{
        void onUserError();
        void onGetUserSuccess();
    }
}
