package com.example.rothnaksomang.mvp.ui.login.interator;

import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.example.rothnaksomang.mvp.entity.User;

import java.util.List;

public class LoginInteratorImp implements Interator {
    @Override
    public User login(User user,List<User> userList, LoginResponceInterator loginResponceInterator) {
        User user2=null;
        Log.e("oooo1"," UserList:"+userList);
        Log.e("oooo1"," User:"+user);
        if(TextUtils.isEmpty(user.getUsername())){
            loginResponceInterator.onUsernameError();
        } else if (TextUtils.isEmpty(user.getPassword())) {
            loginResponceInterator.onPasswordError();
        }else{

            Boolean check=false;
            for (User user1:userList) {
                Log.e("oooo1"," name:"+user1.getUsername()+"=name:"+ user.getUsername());
                Log.e("oooo1"," pwd:"+user1.getPassword()+"=pwd:"+ user.getPassword());
                if(user.getUsername().equals(user1.getUsername()) && user.getPassword().equals(user1.getPassword())){
                    check=true;
                    user2=user1;
                    Log.e("oooo1"," Log true");
                    break;
                }
            }
            if(check==true){
                Log.e("oooo1"," true");
                loginResponceInterator.onLoginSuccess();
            }else {
                Log.e("oooo1"," false:"+userList);
                loginResponceInterator.onLoginNotSuccess();
            }
        }
        return user2;
    }

    @Override
    public void Register(User user, List<User> userList, RegisterResponceInterator registerResponceInterator) {
        if(TextUtils.isEmpty(user.getUsername())){
            registerResponceInterator.onUsernameRegisterError();
        } else if (TextUtils.isEmpty(user.getPassword())) {
            registerResponceInterator.onPasswordRegisterError();
        }else {
            Log.e("ooo1","Size:"+userList.size());
            Boolean check=false;
            for (User user1:userList) {
                if(user.getUsername().equals(user1.getUsername()) && user.getPassword().equals(user1.getPassword())){
                    check=true;
                    Log.e("oooo1","Register true");
                    break;
                }
            }
            if(check==true){
                registerResponceInterator.onOldUser();
            }else {
                registerResponceInterator.onNewUser();
            }
        }
    }

}
