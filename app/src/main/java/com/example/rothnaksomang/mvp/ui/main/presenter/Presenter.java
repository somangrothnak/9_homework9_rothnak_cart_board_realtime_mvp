package com.example.rothnaksomang.mvp.ui.main.presenter;

import com.example.rothnaksomang.mvp.entity.User;

public interface Presenter {
    interface GetUserView{
        void onGetUserError();
        void onGetUserSuccess();
    }
    interface GetUserPresenter{
        void onDestroyView();
        void onGetUser(User user);
    }
}
