package com.example.rothnaksomang.mvp.ui.login.interator;

import com.example.rothnaksomang.mvp.entity.User;

import java.util.List;

public interface Interator {
    User login(User user,List<User> userList, LoginResponceInterator loginResponceInterator);
    void Register(User user, List<User> userList,RegisterResponceInterator registerResponceInterator);

    interface LoginResponceInterator{
        void onUsernameError();
        void onPasswordError();
        void onLoginSuccess();
        void onLoginNotSuccess();
    }
    interface RegisterResponceInterator{
        void onUsernameRegisterError();
        void onPasswordRegisterError();
        void onNewUser();
        void onOldUser();
    }


}
