package com.example.rothnaksomang.mvp.ui.main.presenter;

import com.example.rothnaksomang.mvp.entity.User;
import com.example.rothnaksomang.mvp.ui.main.interator.Interator;
import com.example.rothnaksomang.mvp.ui.main.interator.MainInteratorImp;

public class MainPresenterImp implements Presenter.GetUserPresenter,Interator.GetUserResponceInterator {

    Interator interator;
    Presenter.GetUserView getUserView;

    public MainPresenterImp(Presenter.GetUserView getUserView) {
        this.interator = new MainInteratorImp();
        this.getUserView = getUserView;
    }

    @Override
    public void onDestroyView() {
        if(getUserView!=null){
            getUserView=null;
        }
    }

    @Override
    public void onGetUser(User user) {
        if (user!=null){
            interator.getUser(user,this);
        }
    }

    @Override
    public void onUserError() {
        if(getUserView!=null){
            getUserView.onGetUserError();
        }
    }

    @Override
    public void onGetUserSuccess() {
        if (getUserView!=null){
            getUserView.onGetUserSuccess();
        }

    }
}
