package com.example.rothnaksomang.mvp.ui.login.view;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.rothnaksomang.mvp.R;
import com.example.rothnaksomang.mvp.entity.User;
import com.example.rothnaksomang.mvp.ui.login.presenter.LoginPresenterImp;
import com.example.rothnaksomang.mvp.ui.login.presenter.Presenter;
import com.example.rothnaksomang.mvp.ui.main.view.MainActivity;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;

public class LoginActivity extends AppCompatActivity implements Presenter.LoginView,Presenter.RegisterView {

    Presenter.LoginPresenter loginPresenter;
    Presenter.RegisterPresenter registerPresenter;
    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference;
    List<User> userList;
    User user;

    @BindView(R.id.et_username)
    EditText etUsername;
    @BindView(R.id.et_password)
    EditText etPassword;
    @BindView(R.id.pb_login)
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        progressBar.setVisibility(ProgressBar.INVISIBLE);
        loginPresenter=new LoginPresenterImp((Presenter.LoginView) this);
        registerPresenter=new LoginPresenterImp((Presenter.RegisterView) this);

        userList=new ArrayList<>();
        firebaseDatabase=FirebaseDatabase.getInstance();
        databaseReference=firebaseDatabase.getReference("users");
        getAllUser();
        Log.e("ooo2","UserLIst:"+userList);
    }

    @Override
    public void onShowProgressBar() {
        progressBar.setVisibility(ProgressBar.VISIBLE);
    }

    @Override
    public void onHideProgressBar() {
        progressBar.setVisibility(ProgressBar.INVISIBLE);
    }

    @Override
    public void onUsernameError() {
        etUsername.requestFocus();
        Toasty.error(this,"Username is required",Toast.LENGTH_SHORT,true).show();
    }

    @Override
    public void onPasswordError() {
        etPassword.requestFocus();
        Toasty.error(this,"Password is required",Toast.LENGTH_SHORT,true).show();
    }

    @Override
    public void onLoginSuccess() {
        Intent intent=new Intent(getApplicationContext(),MainActivity.class);
        intent.putExtra("user",getUser());
        startActivity(intent);
    }

    @Override
    public void onLoginNotSuccess() {
        Toasty.success(getApplicationContext(),"Please Register before Login!",Toast.LENGTH_SHORT,true).show();
    }

    @OnClick(R.id.btn_login)
    public void Login(){
        user=new User();
        user=loginPresenter.onLoginCredential(getUser(),userList);
        Log.e("ooo3","Login:"+user);
        Log.e("ooo3","UserList:"+userList);
    }

    @OnClick(R.id.btn_register)
    public void Register(){

        registerPresenter.onRegisterCredential(getUser(),userList);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        loginPresenter.onDestroyView();
    }

    //get value from view and return object User
    public User getUser(){
        User user=new User();
        user.setUsername(etUsername.getText().toString());
        user.setPassword(etPassword.getText().toString());
        return user;
    }

    @Override
    public void onRegisterSuccess() {
        saveUser();
    }

    @Override
    public void onUserError() {
        Toasty.error(getApplicationContext(),"Old User Success",Toast.LENGTH_SHORT,true).show();
    }

    @Override
    public void onUsernameRegisterError() {
        etUsername.requestFocus();
        Toasty.error(getApplicationContext(),"Username is required",Toast.LENGTH_SHORT,true).show();
    }

    @Override
    public void onPasswordRegisterError() {
        etPassword.requestFocus();
        Toasty.error(getApplicationContext(),"Password is required",Toast.LENGTH_SHORT,true).show();
    }

    public void getAllUser(){
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                userList.clear();
                for (DataSnapshot data:dataSnapshot.getChildren()) {
                    User user=data.getValue(User.class);
                    userList.add(user);
                }
                Log.e("ooo2","UserLIst in Method:"+userList);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
    public void saveUser(){
        String id=databaseReference.push().getKey();
        String username=etUsername.getText().toString();
        String password=etPassword.getText().toString();
        user=new User();
        user.setUsername(username);
        user.setPassword(password);
        databaseReference.child(id).setValue(user);
        Toasty.success(getApplicationContext(),"Save New User Success",Toast.LENGTH_SHORT,true).show();
    }
}
