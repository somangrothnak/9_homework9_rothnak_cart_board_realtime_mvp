package com.example.rothnaksomang.mvp.ui.login.presenter;

import android.util.Log;

import com.example.rothnaksomang.mvp.entity.User;
import com.example.rothnaksomang.mvp.ui.login.interator.Interator;
import com.example.rothnaksomang.mvp.ui.login.interator.LoginInteratorImp;

import java.util.List;

public class LoginPresenterImp implements Presenter.LoginPresenter,Interator.LoginResponceInterator,Presenter.RegisterPresenter,Interator.RegisterResponceInterator {

    Interator interator;
    Presenter.LoginView loginView;
    Presenter.RegisterView registerView;

    public LoginPresenterImp(Presenter.LoginView loginView) {
        this.interator = new LoginInteratorImp();
        this.loginView = loginView;
    }
    public LoginPresenterImp(Presenter.RegisterView registerView) {
        this.interator = new LoginInteratorImp();
        this.registerView=registerView;
    }

    @Override
    public void onUsernameError() {
        if(loginView!=null){
            loginView.onHideProgressBar();
            loginView.onUsernameError();
        }
    }

    @Override
    public void onPasswordError() {
        if (loginView!=null){
            loginView.onHideProgressBar();
            loginView.onPasswordError();
        }
    }

    @Override
    public void onUsernameRegisterError() {
        if (registerView!=null){
            registerView.onUsernameRegisterError();
        }
    }

    @Override
    public void onPasswordRegisterError() {
        if (registerView!=null){
            registerView.onPasswordRegisterError();
        }
    }

    @Override
    public void onNewUser() {
        if (registerView!=null){
            registerView.onRegisterSuccess();
        }
    }

    @Override
    public void onOldUser() {
        if (registerView!=null){
            registerView.onUserError();
        }
    }

    @Override
    public void onLoginSuccess() {
        if (loginView!=null){
            loginView.onHideProgressBar();
            loginView.onLoginSuccess();
        }
    }

    @Override
    public void onLoginNotSuccess() {
        if (loginView!=null){
            loginView.onLoginNotSuccess();
        }
    }

    @Override
    public void onDestroyView() {
        if(loginView!=null){
            loginView=null;
        }
    }

    @Override
    public User onLoginCredential(User user,List<User> userList) {
        Log.e("ooo4","size:" + userList.size());
        Log.e("ooo4","user:" + interator.login(user,userList,this));
       return interator.login(user,userList,this);

    }

    @Override
    public void onRegisterCredential(User user, List<User> userList) {
        Log.e("ooo5","size:" + userList.size());
        Log.e("ooo5","User:" + interator.login(user,userList,this));
        interator.Register(user,userList, (Interator.RegisterResponceInterator) this);
    }
}
